CREATE USER IF NOT EXISTS 'catalogue_user' IDENTIFIED BY 'default_password';

GRANT ALL ON socksdb.* TO 'catalogue_user';

CREATE TABLE IF NOT EXISTS sock (
	sock_id varchar(40) NOT NULL, 
	name varchar(20), 
	description varchar(200), 
	price float, 
	count int, 
	image_url_1 varchar(40), 
	image_url_2 varchar(40), 
	PRIMARY KEY(sock_id)
);

CREATE TABLE IF NOT EXISTS tag (
	tag_id MEDIUMINT NOT NULL AUTO_INCREMENT, 
	name varchar(20), 
	PRIMARY KEY(tag_id)
);

CREATE TABLE IF NOT EXISTS sock_tag (
	sock_id varchar(40), 
	tag_id MEDIUMINT NOT NULL, 
	FOREIGN KEY (sock_id) 
		REFERENCES sock(sock_id), 
	FOREIGN KEY(tag_id)
		REFERENCES tag(tag_id)
);

INSERT INTO sock VALUES ("03fef6ac-1896-4ce8-bd69-b798f85c6e0b", "Bottle", "Hygienic bottle:The bottle is made of 100% food grade material & is BPA free ensures a safe drinking. Crystal Clarity", 99.99, 1, "/catalogue/images/bottle-01.png", "/catalogue/images/bottle-01.png");
INSERT INTO sock VALUES ("3395a43e-2d88-40de-b95f-e00e1502085b", "Bottle", "Leak Proof: The bottle has a liquid tight cap that prevents leakage & is unbreakable. Freezer safe: These bottles are compatible with refrigerator & not being curvy the bottle occupies less space.", 18, 438, "/catalogue/images/bottle-02.png", "/catalogue/images/bottle-02.png");
INSERT INTO sock VALUES ("510a0d7e-8e83-4193-b483-e27e09ddc34d", "Mug", "This Mug Is Made Up Of Fine Quality Ceramic Material Which Is Microwave & Dishwasher Safe. Beautiful Printed The Mug Is Permanent And Fade-Proof.",  15, 820, "/catalogue/images/mug-01.png", "/catalogue/images/mug-01.png");
INSERT INTO sock VALUES ("6d62d909-f957-430e-8689-b5129c0bb75e", "T-Shirt", "High quality Printed RoundNeck Full sleevesTshirt direct from the manufacturers. 100% Pure combed 155 - 160 GSM Cotton used.",  17.15, 33, "/catalogue/images/tshirt-01.png", "/catalogue/images/tshirt-01.png");
INSERT INTO sock VALUES ("808a2de1-1aaa-4c25-a9b9-6612e8f29a38", "T-Shirt", "High quality: Gives you perfect fit, comfort feel and handsome look. Trusted brand online and no compromise on quality",  17.32, 738, "/catalogue/images/tshirt-02.png", "/catalogue/images/tshirt-02.png");
INSERT INTO sock VALUES ("208a2de1-1aaa-4c25-a9b9-6612e8f29a38", "T-Shirt", "It gives you perfect fit, comfort feel and handsome look. Trusted brand online and no compromise on quality",  17.32, 738, "/catalogue/images/tshirt-02.png", "/catalogue/images/tshirt-02.png");


INSERT INTO tag (name) VALUES ("brown");
INSERT INTO tag (name) VALUES ("geek");
INSERT INTO tag (name) VALUES ("formal");
INSERT INTO tag (name) VALUES ("blue");
INSERT INTO tag (name) VALUES ("skin");
INSERT INTO tag (name) VALUES ("red");
INSERT INTO tag (name) VALUES ("action");
INSERT INTO tag (name) VALUES ("sport");
INSERT INTO tag (name) VALUES ("black");
INSERT INTO tag (name) VALUES ("magic");
INSERT INTO tag (name) VALUES ("green");

INSERT INTO sock_tag VALUES ("03fef6ac-1896-4ce8-bd69-b798f85c6e0b", "10");
INSERT INTO sock_tag VALUES ("03fef6ac-1896-4ce8-bd69-b798f85c6e0b", "7");
INSERT INTO sock_tag VALUES ("3395a43e-2d88-40de-b95f-e00e1502085b", "1");
INSERT INTO sock_tag VALUES ("3395a43e-2d88-40de-b95f-e00e1502085b", "4");
INSERT INTO sock_tag VALUES ("510a0d7e-8e83-4193-b483-e27e09ddc34d", "8");
INSERT INTO sock_tag VALUES ("510a0d7e-8e83-4193-b483-e27e09ddc34d", "9");
INSERT INTO sock_tag VALUES ("510a0d7e-8e83-4193-b483-e27e09ddc34d", "3");
INSERT INTO sock_tag VALUES ("6d62d909-f957-430e-8689-b5129c0bb75e", "2");
INSERT INTO sock_tag VALUES ("6d62d909-f957-430e-8689-b5129c0bb75e", "9");
INSERT INTO sock_tag VALUES ("808a2de1-1aaa-4c25-a9b9-6612e8f29a38", "4");
INSERT INTO sock_tag VALUES ("808a2de1-1aaa-4c25-a9b9-6612e8f29a38", "6");
INSERT INTO sock_tag VALUES ("808a2de1-1aaa-4c25-a9b9-6612e8f29a38", "7");
INSERT INTO sock_tag VALUES ("808a2de1-1aaa-4c25-a9b9-6612e8f29a38", "3");




